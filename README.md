# uit_peru

## Requisitos

1. Comando curl
2. Comando cat
3. Internet (Para actualizar una vez al año)

## Primeros pasos

Creamos la carpeta oculta en el /home/${USER}/.uit

$ mkdir ~/.uit

Copiamos el archivo a $PATH (Puedes saber la ubicación con echo $PATH)

$ sudo cp uit /usr/local/bin/

## Uso

Ejecutamos escribiendo la palabra uit, actualiza 1 vez al año, muestra el valor de la UIT, y permite calcular en unidades y porcentaje.

$ uit

## Otros

Las unidades se ingresan a partir de 1. Y el porcentaje se ingresa en número de 0 a 100. Puedes usar números decimales incluyendo un punto(.) como separador de los números decimales. 
